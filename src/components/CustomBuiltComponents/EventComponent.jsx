import React from "react";
import {Grid} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";


export default function Event(props) {
    const {event, ...rest} = props;
    const useStyles = makeStyles({
        eventItem: {
            marginTop: '30px',
            padding: '20px'
        },
        itemText: {
            margin: '10px 0px'
        }
    })
    const classes = useStyles();
    return (
        <Grid item container xs={12} sm={12}>
            <Paper elevation={4} className={classes.eventItem}>
                <Grid item container direction="row" xs={12} sm={12}>
                    <Grid item xs={12} sm={4}>
                        <img src={event.eventImage} alt="unable to fetch img." height={200} width={270}/>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Typography variant="h5">
                            {event.eventTitle}
                        </Typography>
                        <Typography variant="caption">
                            {event.eventDescription}
                        </Typography>
                        <br/>
                        <Grid className={classes.itemText}>
                            <Typography variant="caption">
                                <b>Starts at</b>
                            </Typography>
                            <Typography>
                                {event.eventStartDate}
                            </Typography>
                        </Grid>
                        <Grid className={classes.itemText}>
                            <br/>
                            <Typography variant="caption">
                                <b>Ends at</b>
                            </Typography>
                            <Typography>
                                {event.eventEndDate}
                            </Typography>
                        </Grid>
                    </Grid>
                    <Grid item container spacing={3} xs={12} sm={2} justify="flex-end" alignItems="flex-end"
                          direction="column">
                        <Grid item>
                            <Link to="/admin/events/event_id/participations">
                                <Button variant="contained" color="primary">
                                    View participants
                                </Button>
                            </Link>
                        </Grid>
                        <Grid item>
                            <Button variant="outlined" color="primary">
                                Edit
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    )
}