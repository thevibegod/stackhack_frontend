import React from "react";
import {Formik, Field, Form} from "formik";
import * as Yup from "yup";
// import {axios} from "axios";
import {TextField} from "formik-material-ui";
import {Button, Typography} from "@material-ui/core";
import {Container, Grid, makeStyles} from "@material-ui/core";
import {FormControlLabel, Radio, LinearProgress} from '@material-ui/core';
import {RadioGroup} from 'formik-material-ui';
import {DateTimePicker} from 'formik-material-ui-pickers';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const useStyles = makeStyles({
    form_field: {
        marginTop: 50
    },
    buttonComponent: {
        padding: "10px 20px",
        marginTop: 50,
    }
})

const CustomTextField = ({children, ...props}) => {
    const classes = useStyles();
    return (
        <TextField variant="outlined" className={classes.form_field} {...props}>
            {children}
        </TextField>
    )
}

const EventForm = () => {
    const classes = useStyles();
    return (

        <Container maxWidth="sm">
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Typography variant="h4">
                    Register
                </Typography>
                <Formik
                    initialValues={{
                        firstName: "",
                        lastName: "",
                        mobile: "",
                        email:"",
                        registrationType: "self",
                        tickets: 1
                    }}

                    validationSchema={Yup.object().shape({
                        firstName: Yup.string().required('Please Enter your First Name'),
                        lastName:  Yup.string().required('Please Enter your Last Name'),
                        mobile: Yup.string().required('Please Enter your mobile number').matches('/^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/',"Please Enter a Valid Mobile Number"),
                        email: Yup.string().email().required('Please Enter your email'),
                        registrationType: Yup.string().required(),
                        tickets: Yup.number().integer().required().min(1)

                    })}

                    onSubmit={(values, {setSubmitting, resetForm}) => {
                        // axios({
                        //     method: "POST",<Grid className={classes.form_field} item xs={12} sm={6}>
    
    
                                            {/*<Grid item xs={12} sm={6} className={classes.form_field}>*/}
                                            {/*    /!*<Grid item xs={12} sm={4}>*!/*/}
                                            {/*    <Typography variant="subtitle2">*/}
                                            {/*        Ticket Type*/}
                                            {/*    </Typography>*/}
                                            {/*    <Field component={RadioGroup} name="ticket_type">*/}
                                            {/*        <FormControlLabel*/}
                                            {/*            value="SELF"*/}
                                            {/*            control={<Radio disabled={isSubmitting}/>}*/}
                                            {/*            label="self"*/}
                                            {/*            disabled={isSubmitting}*/}
                                            {/*        />*/}
                                            {/*        <FormControlLabel*/}
                                            {/*            value="CORPORATE"*/}
                                            {/*            control={<Radio disabled={isSubmitting}/>}*/}
                                            {/*            label="corporate"*/}
                                            {/*            disabled={isSubmitting}*/}
                                            {/*        />*/}
                                            {/*        <FormControlLabel*/}
                                            {/*            value="OTHERS"*/}
                                            {/*            control={<Radio disabled={isSubmitting}/>}*/}
                                            {/*            label="others"*/}
    
                                            {/*            disabled={isSubmitting}*/}
                                            {/*        />*/}
                                            {/*    </Field>*/}
                                            {/*</Grid>*/}
                        //     url: `http://localhost:8000/events/create`,
                        //     data: values,
                        //     headers: {
                        //         /* TODO : to replace access token */
                        //         'authToken': `TODO:to be replaced.`
                        //     }
                        // }).then(response => {
                        //     if (response.status === 200) {
                        //         setSubmitting(false)
                        //     }
                        // })
                    }}
                >
                    {
                        ({values, isSubmitting, handleSubmit, handleChange, handleBlur, submitForm}) => {
                            return (

                                <Form>
                                    <Grid container direction="row" justify='center' alignContent="center">
                                        <Grid item xs={12} sm={6} alignItems="flex-end">
                                            <Field
                                                component={CustomTextField}
                                                name="firstName"
                                                type="text"
                                                label="First Name"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Field
                                                component={CustomTextField}
                                                name="lastName"
                                                label="Last Name"
                                                type="text"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Field
                                                component={CustomTextField}
                                                name="mobile"
                                                label="Mobile Number"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Field
                                                component={CustomTextField}
                                                name="email"
                                                label="Email"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                         
                                        <Field
                                                component={RadioGroup}
                                                name="registrationType"
                                                label="Ticket Type"
                                        >
                                            <Typography variant="subtitle2">
                                            Ticket Type
                                        </Typography>
                                        <FormControlLabel
                                                value="SELF"
                                                control={<Radio disabled={isSubmitting}/>}
                                                label="self"
                                                checked
                                                disabled={isSubmitting}
                                                />
                                        <FormControlLabel
                                                value="GROUP"
                                                control={<Radio disabled={isSubmitting}/>}
                                                label="group"
                                                disabled={isSubmitting}
                                                />
                                        <FormControlLabel
                                                value="CORPORATE"
                                                control={<Radio disabled={isSubmitting}/>}
                                                label="corporate"
                                                disabled={isSubmitting}
                                                />
                                        <FormControlLabel
                                                value="OTHERS"
                                                control={<Radio disabled={isSubmitting}/>}
                                                label="others"
                                                disabled={isSubmitting}
                                                />        
                                        </Field>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                        <Field
                                                component={CustomTextField}
                                                name="tickets"
                                                label="Number of Tickets"
                                                type="number"
                                            />
                                        </Grid>

                                        <Grid item xs={12} sm={6}>
                                            <Button
                                                className={classes.buttonComponent}
                                                variant="contained"
                                                color="primary"
                                                disabled={isSubmitting}
                                                onClick={submitForm}
                                            >
                                                <Typography variant="button">
                                                    Submit
                                                </Typography>
                                            </Button>
                                        </Grid>


                                    </Grid>
                                </Form>
                            )
                        }
                    }

                </Formik>


            </MuiPickersUtilsProvider>
        </Container>
    )

};

export default EventForm;