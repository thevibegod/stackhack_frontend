import React from "react"
import {Container} from "@material-ui/core"
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

export default function ParticipationHeader(props) {
    return (
        <Container maxWidth="sm">
            <Grid container direction="column">
                <Grid item>
                    <Paper>
                        <img src={props.event_image} alt="Event Image"/>
                    </Paper>
                </Grid>
                Grid
            </Grid>
        </Container>
    )
}