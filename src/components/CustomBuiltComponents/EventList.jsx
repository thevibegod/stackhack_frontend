import React, {useEffect, useState} from "react";
import {Grid, Button, Typography, Container} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import EventComponent from "./EventComponent";
import eventImg from "./EVENT_IMAGE_CONSTANT"


// Mock data for initializing the state. Actual data should come from the BACKEND
const initialState = [
    {
        'eventImage': eventImg,
        'eventTitle': 'KubeCon 2020 (Online)',
        'eventDescription': 'KubeCon is a global developer conference that aims at providing knowledge regarding depoyment of web applications to developers through best practices',
        'eventVenue': '12, Saravanampatti main Road, Coimbatore 641049',
        'eventStartDate': new Date(Date.now()).toString(),
        'eventEndDate': new Date(Date.now()).toString(),
        'acceptFrom': new Date(Date.now()).toString(),
        'acceptUntil': new Date(Date.now()).toString(),
        'maxParticipants': 250,
        'shouldRender': true
    },
    {
        'eventImage': eventImg,
        'eventTitle': 'KubeCon 2020 (Online)',
        'eventDescription': 'KubeCon is a global developer conference that aims at providing knowledge regarding depoyment of web applications to developers through best practices',
        'eventVenue': '12, Saravanampatti main Road, Coimbatore 641049',
        'eventStartDate': new Date(Date.now()).toString(),
        'eventEndDate': new Date(Date.now()).toString(),
        'acceptFrom': new Date(Date.now()).toString(),
        'acceptUntil': new Date(Date.now()).toString(),
        'maxParticipants': 250,
        'shouldRender': true
    },
    {
        'eventImage': eventImg,
        'eventTitle': 'KubeCon 2020 (Online)',
        'eventDescription': 'KubeCon is a global developer conference that aims at providing knowledge regarding depoyment of web applications to developers through best practices',
        'eventVenue': '12, Saravanampatti main Road, Coimbatore 641049',
        'eventStartDate': new Date(Date.now()).toString(),
        'eventEndDate': new Date(Date.now()).toString(),
        'acceptFrom': new Date(Date.now()).toString(),
        'acceptUntil': new Date(Date.now()).toString(),
        'maxParticipants': 250,
        'shouldRender': true
    }
]


// Actual React Component for rendering out the events
export default function EventList(props) {
    const useStyles = makeStyles({
        jumbo: {
            margin: '5% 0',
            width: '100%'
        }
    })
    const classes = useStyles()
    const [events, setEvents] = useState(initialState)


    useEffect(() => {
        //    TODO : Make an API call to the BACKEND for listing events
    }, [])

    return (
        <Grid container direction='column'>
            <Grid item container className={classes.jumbo} justify="center" alignItems="center" xs={12} sm={12}>
                <Typography variant='h4'>
                    Upcoming Events
                </Typography>
            </Grid>
            <Grid item container xs={12} sm={12} direction='column' justify='center' alignItems='center'>
                {events.map(event => {
                    return (
                        <EventComponent event={event}/>
                    )
                })}
            </Grid>
        </Grid>
    )
}
