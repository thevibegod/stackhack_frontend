import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from "@material-ui/core/ListSubheader";
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import EventOutlinedIcon from '@material-ui/icons/EventOutlined';
import {NavLink} from "react-router-dom";

export const mainListItems = (
    <div>
        <NavLink to="/admin/events/create" style={{textDecoration:'None',color:'black'}}>
            <ListItem button>
                <ListItemIcon>
                    <AddOutlinedIcon/>
                </ListItemIcon>
                <ListItemText primary="Add Event"/>
            </ListItem>
        </NavLink>
        <NavLink to="/admin/events/upcoming" style={{textDecoration:'None',color:'black'}}>
            <ListItem button>
                <ListItemIcon>
                    <EventOutlinedIcon/>
                </ListItemIcon>
                <ListItemText primary="Upcoming Events"/>
            </ListItem>
        </NavLink>
    </div>
);
