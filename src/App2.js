import React from "react";
import Dashboard from "./components/dashboard_components/Dashboard";
import EventForm from "./components/CustomBuiltComponents/EventForm.jsx"
import {BrowserRouter as Router, Switch, Link, Route} from "react-router-dom";
import EventList from "./components/CustomBuiltComponents/EventList";
import SignIn from "./components/CustomBuiltComponents/SignIn";
import PasswordChangeForm from "./components/CustomBuiltComponents/PasswordChange";
import ParticipantsList from "./components/CustomBuiltComponents/ParticipantsList";
import RegistrationForm from "./components/CustomBuiltComponents/RegistrationForm";

export default (props) => {
    return (
        <>

            <Router>
                <Dashboard>
                    <Switch>
                        <Route path='/register'>
                            <RegistrationForm/>
                        </Route>
                        <Route path='/admin/events/create'>
                            <EventForm/>
                        </Route>
                        <Route path='/admin/events/upcoming'>
                            <EventList/>
                        </Route>
                        <Route path='/admin/events/event_id/participations'>
                            <ParticipantsList/>
                        </Route>
                        <Route path='/admin/changepassword'>
                            <PasswordChangeForm/>
                        </Route>
                        <Route path='/admin/events/create'>
                            <EventForm/>
                        </Route>
                        <Route path='/admin'>
                            <SignIn/>
                        </Route>
                    </Switch>
                </Dashboard>
            </Router>
        </>
    )
}