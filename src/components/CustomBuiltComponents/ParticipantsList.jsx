import React from "react"
import MaterialTable from "material-table"
import {forwardRef} from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref}/>),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref}/>),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref}/>),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref}/>),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref}/>),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref}/>),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref}/>),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref}/>),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref}/>),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref}/>),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref}/>),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref}/>),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref}/>),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref}/>),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref}/>),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref}/>),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref}/>),
    VisibilityOutlinedIcon: forwardRef((props, ref) => <VisibilityOutlinedIcon {...props} ref={ref}/>),
};


export default function ParticipantsList(props) {
    return (
        <MaterialTable
            title="Participation List"
            columns={[
                {title: 'Name', field: 'name'},
                {title: 'Email', field: 'email'},
                {title: 'Mobile', field: 'mobile'},
                {title: 'Type', field: 'reg_type'},
                {title: 'No. of Tickets', field: 'ticket_count', type: 'numeric'},
            ]}
            icons={tableIcons}
            data={[
                {name: 'Adarsh', email: 'adarshfrompupil@gmail.com', mobile: "7904273130", reg_type: "SELF",ticket_count:1},
                {name: 'Aswath', email: 'aswath7862001@gmail.com', mobile: "8838034518", reg_type: "CORPORATE",ticket_count: 3},
            ]}
            options={{
                sorting: true
            }}
            actions={[
                {
                    icon: ()=>(<VisibilityOutlinedIcon/>),
                    tooltip: 'Veiw ID',
                    onClick: (event, rowData) => {
                        // Do save operation
                        alert("id will appear")
                    }
                }
            ]}
        />
    )
}