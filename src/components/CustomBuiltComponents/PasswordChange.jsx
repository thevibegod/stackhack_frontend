import React from "react";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
// import {axios} from "axios";
import { TextField } from "formik-material-ui";
import { Button, Typography } from "@material-ui/core";
import { Container, Grid, makeStyles } from "@material-ui/core";
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const useStyles = makeStyles({
    form_field: {
        marginTop: 50
    },
    buttonComponent: {
        padding: "10px 20px",
        marginTop: 50,
    }
})

const CustomTextField = ({ children, ...props }) => {
    const classes = useStyles();
    return (
        <TextField variant="outlined" className={classes.form_field} {...props}>
            {children}
        </TextField>
    )
}
const PasswordChangeForm = () => {
    const classes = useStyles();
    return (

        <Container maxWidth="sm">
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Typography variant="h4">
                    Change Password
                </Typography>
                <Formik
                    initialValues={{
                        old_password: '',
                        new_password: '',
                        confirm_password: ''
                    }}

                    validationSchema={Yup.object().shape({
                        old_password: Yup.string().required("Please enter your old password"),
                        new_password: Yup.string().required("Please enter a new password").min(8, "Enter atleast 8 characters"),
                        confirm_password: Yup.string().oneOf([Yup.ref('new_password'), null], 'Passwords must match').required("Please enter the new password again").min(8, "Enter atleast 8 characters")
                    })}

                    onSubmit={(values, { setSubmitting, resetForm }) => {
                        // axios({
                        //     method: "POST",
                        //     url: `http://localhost:8000/events/create`,
                        //     data: values,
                        //     headers: {
                        //         /* TODO : to replace access token */
                        //         'authToken': `TODO:to be replaced.`
                        //     }
                        // }).then(response => {
                        //     if (response.status === 200) {
                        //         setSubmitting(false)
                        //     }
                        // })
                    }}
                >
                    {
                        ({ values, isSubmitting, handleSubmit, handleChange, handleBlur, submitForm }) => {
                            return (

                                <Form>
                                    <Grid container direction="row" justify='center' alignContent="center">
                                        <Grid item xs={10} sm={12} alignItems="flex-end">
                                            <Field
                                                component={CustomTextField}
                                                name="old_password"
                                                type="password"
                                                label="Old Password"
                                            />
                                        </Grid>
                                    </Grid>
                                    <Grid container direction="row" justify='center' alignContent="center">
                                        <Grid item xs={10} sm={12}>
                                            <Field
                                                component={CustomTextField}
                                                name="new_password"
                                                type="password"
                                                label="New Password"
                                            />
                                        </Grid>
                                        </Grid>
                                        <Grid container direction="row" justify='center' alignContent="center">
                                        <Grid item xs={10} sm={12}>
                                            <Field
                                                component={CustomTextField}
                                                name="confirm_password"
                                                type="password"
                                                label="Confirm New Password"
                                            />
                                        </Grid>

                                        </Grid>
                                        <Grid container direction="row" justify='center' alignContent="center">
                                        <Grid item xs={10} sm={12}>
                                            <Button
                                                className={classes.buttonComponent}
                                                variant="contained"
                                                color="primary"
                                                disabled={isSubmitting}
                                                onClick={submitForm}
                                            >
                                                <Typography variant="button">
                                                    Submit
                                                </Typography>
                                            </Button>
                                        </Grid>


                                    </Grid>
                                </Form>
                            )
                        }
                    }

                </Formik>


            </MuiPickersUtilsProvider>
        </Container>
    )

};

export default PasswordChangeForm;