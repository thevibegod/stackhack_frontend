import React from "react";
import {Grommet, Form, FormField, TextInput, TextArea,Calendar} from "grommet";


export default (props) => {
    return (
        <React.Fragment>
            <Form>
                <FormField name="EventTitle" label="Title" component={TextInput} />
                <FormField name="EventDescription" label="Description" component={TextArea}/>
                <FormField name="Venue" label="Venue" component={TextInput}/>
                <Calendar/>
                <Calendar/>
                <FormField name="MaxParticipants" label="Maximum No. of Participants" type="Number"/>
            </Form>
        </React.Fragment>
    )
}