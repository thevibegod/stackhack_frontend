import React, {useState} from 'react';
import Dashboard from "./components/dashboard_components/Dashboard";
import SideBar from "./components/Grommet_components/sidenav"
import {Box, Button, Collapsible, Heading, Grommet} from 'grommet';
import {Notification} from 'grommet-icons';
import EventForm from "./components/Grommet_components/EventForm";

const AppBar = (props) => (
    <Box
        tag='header'
        direction='row'
        align='center'
        justify='between'
        background='brand'
        pad={{left: 'medium', right: 'small', vertical: 'small'}}
        elevation='medium'
        style={{zIndex: '1'}}
        {...props}
    />
);


function App() {
    const [showSidebar, setShowSidebar] = useState(false);

    const theme = {
        global: {
            font: {
                family: 'Roboto',
                size: '18px',
                height: '20px',
            },
        },
    };

    return (
        <Grommet theme={theme}>
            <Box fill>
                {/*<AppBar>*/}
                {/*    /!*    Stack Hack Theme 2*!/*/}
                {/*    <Heading level='3' margin='none'>Stack Hack Theme 2</Heading>*/}
                {/*    <Button*/}
                {/*        icon={<Notification/>}*/}
                {/*        onClick={() => setShowSidebar(!showSidebar)}*/}
                {/*    />*/}
                {/*</AppBar>*/}
                {/*<Box direction='row' flex overflow={{horizontal: 'hidden'}}>*/}
                {/*    <Box flex align='center' justify='center'>*/}
                {/*        app body*/}
                {/*    </Box>*/}
                {/*    {showSidebar && (*/}
                {/*        <Collapsible direction="horizontal" open={showSidebar}>*/}
                {/*            /!*<Box*!/*/}
                {/*            /!*    width='medium'*!/*/}
                {/*            /!*    background='light-2'*!/*/}
                {/*            /!*    elevation='small'*!/*/}
                {/*            /!*    align='center'*!/*/}
                {/*            /!*    justify='center'*!/*/}
                {/*            /!*>*!/*/}
                {/*            /!*    sidebar*!/*/}
                {/*            /!*</Box>*!/*/}
                {/*        */}
                {/*        </Collapsible>*/}
                {/*    )}*/}
                {/*</Box>*/}
                <Box direction="row" pad="large">
                    <SideBar/>
                    <Box justify="center">
                        <EventForm/>
                    </Box>
                </Box>
            </Box>

        </Grommet>
    );
}

export default App;
