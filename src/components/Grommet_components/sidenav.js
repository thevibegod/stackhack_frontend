import React from "react";
import {Sidebar, Nav, Button, Heading, Text, Box} from "grommet";
import {Add, Calendar, History, InProgress} from "grommet-icons";

const SideBarHeader = () => {
    return (
        <Heading>
            Evently
        </Heading>
    )
}

const MenuItems = [
    {
        'itemText': 'Add Event',
        'icon': <Add/>
    },
    {
        'itemText': 'Happening Now',
        'icon': <InProgress color="black"/>
    },
    {
        'itemText': 'Lined Up Events',
        'icon': <Calendar/>
    },
    {
        'itemText': 'Past Events',
        'icon': <History/>
    }
]

export default (props) => {
    return (
        <Box pad="medium">
            <Sidebar header={<SideBarHeader/>}>
                <Nav>
                    {
                        MenuItems.map((item, index) => {
                            return (
                                <React.Fragment key={index}>
                                    <Button icon={item.icon} alignSelf="center" style={{marginTop: '20px'}} hoverIndicator>
                                    </Button>
                                </React.Fragment>
                            )
                        })
                    }
                </Nav>
            </Sidebar>
        </Box>
    )
}