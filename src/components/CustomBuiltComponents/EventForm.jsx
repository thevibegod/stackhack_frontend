import React,{useRef,useState} from "react";
import {Formik, Field, Form} from "formik";
import * as Yup from "yup";
// import {axios} from "axios";
import {TextField} from "formik-material-ui";
import {Button, Typography} from "@material-ui/core";
import {Container, Grid, makeStyles} from "@material-ui/core";
import {FormControlLabel, Radio, LinearProgress} from '@material-ui/core';
import {RadioGroup} from 'formik-material-ui';
import {DateTimePicker} from 'formik-material-ui-pickers';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const useStyles = makeStyles({
    form_field: {
        marginTop: 50
    },
    buttonComponent: {
        padding: "10px 20px",
        marginTop: 50,
    }
})

const CustomTextField = ({children, ...props}) => {
    const classes = useStyles();
    return (
        <TextField variant="outlined" className={classes.form_field} {...props}>
            {children}
        </TextField>
    )
}

const CustomTextFieldMultiline = ({children, ...props}) => {
    const classes = useStyles();
    return (

        <TextField variant="outlined" className={classes.form_field} multiline {...props}>
            {children}
        </TextField>
    )
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

const EventForm = () => {
    const classes = useStyles();
    const imageReference = useRef();
    const [eventImageDecoded,setEventImageDecoded] = useState("");
    return (

        <Container maxWidth="sm">
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Typography variant="h4">
                    Create Event
                </Typography>
                <Formik
                    initialValues={{
                        eventTitle: "",
                        eventDescription: "",
                        venue_address: "",
                        venueCoords: [],
                        startDate: new Date(Date.now()),
                        endDate: new Date(Date.now()),
                        maxParticipants: 1,
                        acceptFrom: new Date(Date.now()),
                        acceptUntil: new Date(Date.now()),
                        shouldRender: false,
                    }}

                    validationSchema={Yup.object().shape({
                        eventTitle: Yup.string().min(5, "Should be 5 characters or more").max(40, "Should be 40 characters or less").required("Please provide title"),
                        eventDescription: Yup.string().min(10, "Description should at least be 10 characters").required("Please provide description"),
                        venue_address: Yup.string().required("Event venue cannot be null"),
                        startDate: Yup.date().required(),
                        endDate: Yup.date().required(),
                        maxParticipants: Yup.number().min(5, "No. of participants should atleast be 5").required(),
                        acceptFrom: Yup.date().required(),
                        acceptUntil: Yup.date().required(),
                        shouldRender: Yup.boolean()
                    })}

                    onSubmit={(values, {setSubmitting, resetForm}) => {
                        // axios({
                        //     method: "POST",
                        //     url: `http://localhost:8000/events/create`,
                        //     data: values,
                        //     headers: {
                        //         /* TODO : to replace access token */
                        //         'authToken': `TODO:to be replaced.`
                        //     }
                        // }).then(response => {
                        //     if (response.status === 200) {
                        //         setSubmitting(false)
                        //     }
                        // })
                        console.log(values['eventImage'])
                        alert(JSON.stringify(values),null,2);
                    }}
                >
                    {
                        ({values, isSubmitting, handleSubmit, handleChange, handleBlur, submitForm, setFieldValue}) => {
                            return (

                                <Form>
                                    <Grid container direction="row" justify='flex-start' alignContent="flex-start">
                                        <Grid item xs={12} sm={6} className={classes.form_field}>
                                            <input id="file" style={{display:"none"}} name="eventImage" type="file" onChange={(event) => {
                                                getBase64(event.currentTarget.files[0]).then(string=>{
                                                    setEventImageDecoded(string)
                                                })
                                                // setEventImageDecoded(getBase64((event.currentTarget.files[0])).toString())
                                                console.log(eventImageDecoded)
                                                setFieldValue("eventImage", event.currentTarget.files[0]);
                                            }} accept="image/x-png,image/gif,image/jpeg" ref={imageReference}/>
                                            <Button variant="contained" onClick={()=>{console.log(imageReference);imageReference.current.click();}}>Upload Image</Button>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <img src={eventImageDecoded} width={200} height={150} alt=""/>
                                        </Grid>
                                        <Grid item xs={12} sm={6} alignItems="flex-end">
                                            <Field
                                                component={CustomTextField}
                                                name="eventTitle"
                                                type="text"
                                                label="Title"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Field
                                                component={CustomTextFieldMultiline}
                                                name="eventDescription"
                                                label="Description"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Field
                                                component={CustomTextFieldMultiline}
                                                name="venue_address"
                                                label="Venue"
                                            />
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <Field
                                                component={CustomTextField}
                                                name="maxParticipants"
                                                label="Maximum Participants"
                                            />
                                        </Grid>
                                        <Grid className={classes.form_field} item xs={12} sm={6}>
                                            <Field component={DateTimePicker} label="Start Date" name="startDate"/>
                                        </Grid>
                                        <Grid className={classes.form_field} item xs={12} sm={6}>
                                            <Field component={DateTimePicker} label="End Date" name="endDate"/>
                                        </Grid>
                                        <Grid className={classes.form_field} item xs={12} sm={6}>
                                            <Field component={DateTimePicker} label="Accept Participants From"
                                                   name="acceptFrom"/>
                                        </Grid>
                                        <Grid className={classes.form_field} item xs={12} sm={6}>
                                            <Field component={DateTimePicker} label="Accept Participants Until"
                                                   name="acceptUntil"/>
                                        </Grid>


                                        {/*<Grid item xs={12} sm={6} className={classes.form_field}>*/}
                                        {/*    /!*<Grid item xs={12} sm={4}>*!/*/}
                                        {/*    <Typography variant="subtitle2">*/}
                                        {/*        Ticket Type*/}
                                        {/*    </Typography>*/}
                                        {/*    <Field component={RadioGroup} name="ticket_type">*/}
                                        {/*        <FormControlLabel*/}
                                        {/*            value="SELF"*/}
                                        {/*            control={<Radio disabled={isSubmitting}/>}*/}
                                        {/*            label="self"*/}
                                        {/*            disabled={isSubmitting}*/}
                                        {/*        />*/}
                                        {/*        <FormControlLabel*/}
                                        {/*            value="CORPORATE"*/}
                                        {/*            control={<Radio disabled={isSubmitting}/>}*/}
                                        {/*            label="corporate"*/}
                                        {/*            disabled={isSubmitting}*/}
                                        {/*        />*/}
                                        {/*        <FormControlLabel*/}
                                        {/*            value="OTHERS"*/}
                                        {/*            control={<Radio disabled={isSubmitting}/>}*/}
                                        {/*            label="others"*/}

                                        {/*            disabled={isSubmitting}*/}
                                        {/*        />*/}
                                        {/*    </Field>*/}
                                        {/*</Grid>*/}

                                        <Grid item xs={12} sm={6} justify="flex-start" alignItems="flex-start">
                                            <Button
                                                className={classes.buttonComponent}
                                                variant="contained"
                                                color="primary"
                                                disabled={isSubmitting}
                                                onClick={submitForm}
                                            >
                                                <Typography variant="button">
                                                    Submit
                                                </Typography>
                                            </Button>
                                        </Grid>


                                    </Grid>
                                </Form>
                            )
                        }
                    }

                </Formik>


            </MuiPickersUtilsProvider>
        </Container>
    )

};

export default EventForm;